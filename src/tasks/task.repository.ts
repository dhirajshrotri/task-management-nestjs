import { Repository, EntityRepository } from "typeorm";
import {Task} from './tasks.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatus } from './task-status.enum';
import { NotFoundException } from "@nestjs/common";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {
    async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
        const task = new Task();

        const {title, description } = createTaskDto;
        task.title = title, task.description = description;
        task.status = TaskStatus.OPEN;

        await task.save();

        return task;
    }

    async deleteTask(id: number): Promise<void> {
        const result = await Task.delete(id);
        
        if(result.affected === 0) {
            throw new NotFoundException(`The task with "${id}" was not found.`)
        }
    }

    async getTasks(filterDto: GetTasksFilterDto): Promise<Task[]> {
        const { status, search } = filterDto;
        const query = this.createQueryBuilder('task');

        const tasks = await query.getMany();
        return tasks;
    }
}