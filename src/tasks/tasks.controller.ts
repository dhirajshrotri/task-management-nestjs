import { Controller, Get, Post, Body, Param, Delete, Patch, Query, UsePipes, ValidationPipe, ParseIntPipe } from '@nestjs/common';
import {TasksService} from './tasks.service';
import {TaskStatus} from './task-status.enum'
import {CreateTaskDto} from './dto/create-task.dto'
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { Task } from './tasks.entity'
import { TaskStatusValidationPipe } from './pipe/TaskStatusValidationPipe';
@Controller('tasks')
export class TasksController {
    constructor(private tasksService: TasksService) {

    }

    @Get() //to make sure that get requests are handled by getAllTasks()
    getTasks (@Query(ValidationPipe) filterDto: GetTasksFilterDto){
        return this.tasksService.getTasks(filterDto); //response of the endpoint
    }

    @Get('/:id') 
    getTaskById (@Param('id', ParseIntPipe) id: number): Promise<Task> {
        return this.tasksService.getTaskById(id);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createTask(
        @Body() createTaskDto: CreateTaskDto
     ): Promise<Task> {
        return this.tasksService.createTask(createTaskDto)
    }

    @Delete('/:id')
    deleteTaskById(
        @Param('id', ParseIntPipe) id: number
    ): Promise<void> {
        return this.tasksService.deleteTaskById(id);
    }

    @Patch('/:id/status')
    updateTaskStatusById(
        @Param('id', ParseIntPipe) id: number,
        @Body('status', TaskStatusValidationPipe) status: TaskStatus
    ):Promise<Task> {
        return this.tasksService.updateTaskStatusById(id, status);
    }
}
