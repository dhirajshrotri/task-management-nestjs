import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskRepository } from './task.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './tasks.entity';
import { TaskStatus } from './task-status.enum';

@Injectable()
export class TasksService {

    constructor (
        @InjectRepository(TaskRepository)
        private taskRepository: TaskRepository,
    ) {};

    async getTaskById (id: number): Promise<Task>{
        const found = await this.taskRepository.findOne(id);

        if(!found) {
            throw new NotFoundException(`Task with ${id} not found.`);
        }

        return found;
    }

    async createTask(createTaskDto: CreateTaskDto) {
        return this.taskRepository.createTask(createTaskDto);
    }
    
    deleteTaskById(id: number): Promise<void> {
        return this.taskRepository.deleteTask(id)
    }
  
    async updateTaskStatusById(id: number, status: TaskStatus) :Promise<Task>{
        const task = await this.getTaskById(id);

        task.status = status;

        await task.save();

        return task;
    }

    async getTasks(filterDto: GetTasksFilterDto): Promise<Task[]> {
        
       return await this.taskRepository.getTasks(filterDto);
       
    }
}
