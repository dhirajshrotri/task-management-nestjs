import {TypeOrmModuleOptions} from '@nestjs/typeorm';
import {Task} from '../tasks/tasks.entity'
export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: '172.17.0.4',
    port: 5432,
    username: 'postgres',
    password: 'postgres',
    database: 'taskmanagement',
    entities: [
        Task
    ],
    synchronize: true,
};